from downloader.downloader import generator_text_files_lines, download_urls
from downloader.downloader import unzip_tgz_folders
from itertools import takewhile
from glob import glob

import inquirer
import logging
import sys
import os

URL_LIST = [
    'http://image-net.org/imagenet_data/urls/imagenet_fall11_urls.tgz',
    'http://image-net.org/imagenet_data/urls/imagenet_winter11_urls.tgz',
    'http://image-net.org/imagenet_data/urls/imagenet_spring10_urls.tgz',
    'http://image-net.org/imagenet_data/urls/imagenet_fall09_urls.tgz',
]

DATA_FOLDER = 'data'


def download_urls_texts():
    filepaths = download_urls(URL_LIST, DATA_FOLDER, log)
    unzip_tgz_folders(filepaths, DATA_FOLDER, log)


def get_text_files_line_generator():
    text_filepaths = glob(os.path.join(DATA_FOLDER, 'texts', '*.txt'))
    return generator_text_files_lines(text_filepaths[0])


def main(log):
    """
    Main program of downloading image files.
    """

    actions = {
        'nothing': lambda: print("Nothing"),
        'dowload_urls': download_urls_texts,
        'get_generator': lambda: print(get_text_files_line_generator)
    }

    questions = [
        inquirer.List(
            'action',
            message="What action would you like to take?",
            choices=actions.keys(),
        ),
    ]

    # Get answer from user
    answer = inquirer.prompt(questions).get('action', 'nothing')

    # Call function
    actions[answer]()


if __name__ == "__main__":

    # Specify format for logging
    format = '%(asctime)s - %(message)s'

    # Set logging config
    logging.basicConfig(stream=sys.stdout,
                        level=logging.INFO,
                        format=format)

    # Get logger
    log = logging.getLogger(__name__)

    try:
        main(log)
    except KeyboardInterrupt:
        print("You stopped the program")
