
from pathos.multiprocessing import ProcessingPool as Pool
from downloader import utils

import tarfile
import os


def download_urls(urls, data_folder, log):
    """
    Downloads url zips.
    """
    pool = Pool()

    def f(url):
        local_filename = url.split('/')[-1]
        dest = os.path.join(data_folder, 'tars', local_filename)
        if os.path.exists(dest):
            log.info(f"Already exists: {url}")
            return dest
        log.info(f"Downloading: {url}")
        return utils.download(url, dest)

    log.info("Starting download, don't quit the program:")

    return pool.map(f, urls)


def unzip_tgz_folders(filepaths, data_folder, log):
    """
    Unzip tgz folders in data folder.
    """

    folder = os.path.join(data_folder, 'texts')
    if os.listdir(folder):
        log.info(f"Unzipped text files exist in {folder}")
        return

    pool = Pool()

    log.info("Unzipping tgz in data folder")

    def f(filepath):
        tar = tarfile.open(filepath)
        tar.extractall(data_folder)
        tar.close()

    pool.map(f, filepaths)


def generator_text_files_lines(filepaths):
    """
    Yield lines in big text files.
    """
    for fname in filepaths:
        with open(fname) as infile:
            for line in infile:
                yield line
