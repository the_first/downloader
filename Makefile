
help:
	@echo "Tasks in \033[1;32mdownloader\033[0m:"
	@cat Makefile

install:
	export PIPENV_VENV_IN_PROJECT=1
	pipenv install -d

lint:
	mypy src --ignore-missing-imports
	flake8 src --ignore=$(shell cat flake_ignored)

dev:
	python setup.py develop

test: dev
	pytest tests/ --doctest-modules --junitxml=junit/test-results.xml

build: clean
	pip install wheel
	python setup.py bdist_wheel

clean:
	@rm -rf .pytest_cache/ .mypy_cache/ junit/ build/ dist/
	@rm -rf cfg/ data/ models/
	@find . -not -path './.venv*' -path '*/__pycache__*' -delete
	@find . -not -path './.venv*' -path '*/*.egg-info*' -delete

dockerize: build
	docker build --rm -f "Dockerfile" -t menziess/downloader:latest .

publish:
	docker push menziess/downloader:latest

run:
	@echo "Running docker container using env_file"
	docker run --rm -it \
	--env-file=env_file \
	menziess/downloader:latest
